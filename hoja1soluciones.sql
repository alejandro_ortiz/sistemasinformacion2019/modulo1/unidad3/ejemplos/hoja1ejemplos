﻿DROP DATABASE IF EXISTS ejemploprogramacion1;
CREATE DATABASE ejemploprogramacion1;
USE ejemploprogramacion1;

-- ej1v1. Realizar un procedimiento almacenado que reciba dos números 
-- y te indique el mayor de ellos, realizarle con instrucción if.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1v1(numero1 int, numero2 int)
    BEGIN
      -- declarar variable
      DECLARE vmayor int DEFAULT 0; 

      -- realizar calculos
      IF numero1 > numero2 THEN
        SET vmayor = numero1;
      ELSE
        SET vmayor = numero2;
      END IF;

      -- mostrar resultado
      SELECT vmayor;
    END //
  DELIMITER ;

  CALL ej1v1(12,23);

-- ej1v2. Realizar un procedimiento almacenado que reciba dos números 
-- y te indique el mayor de ellos, realizarle con una tabla temporal 
-- y una consulta de totales.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1v2(numero1 int, numero2 int)
    BEGIN
      -- crear tabla tamporal
      CREATE OR REPLACE TEMPORARY TABLE ej1(
        id int AUTO_INCREMENT,
        num int,
        PRIMARY KEY (id)
      );

      -- insertar los datos
      INSERT INTO ej1 (num)
        VALUES (numero1),(numero2);

      -- calcular y mostrar el mayor de los dos
      SELECT MAX(e.num) 
        FROM ej1 e;

    END //
  DELIMITER ;

  CALL ej1v2(12,23);

-- ej1v3. Realizar un procedimiento almacenado que reciba dos números 
-- y te indique el mayor de ellos, realizarle con la función de
-- mysql GREATEST.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1v3(numero1 int, numero2 int)
    BEGIN
      -- declarar variable
      DECLARE vmayor int DEFAULT 0;

      -- realizar calculos
      SET vmayor = GREATEST(numero1, numero2);

      -- mostrar resultado
      SELECT vmayor;
    END //
  DELIMITER ;

  CALL ej1v3(12,23);

-- ej2v1. Realizar un procedimiento almacenado que reciba tres números 
-- y te indique el mayor de ellos, realizarle con instrucción if.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v1(numero1 int, numero2 int, numero3 int)
    BEGIN
      -- declarar variable
      DECLARE vmayor int DEFAULT 0;

      -- realizar calculos
      IF numero1 > numero2 THEN
        IF numero1 > numero3 THEN
          SET vmayor = numero1;
        ELSE
          SET vmayor = numero3;
        END IF;
      ELSE
        IF numero3 > numero2 THEN
          SET vmayor = numero3 ;
        ELSE
          SET vmayor = numero2;
        END IF;
      END IF;

      -- mostrar resultado
      SELECT vmayor;

    END //
  DELIMITER ;

  CALL ej2v1(12,23,55); 
  CALL ej2v1(12,73,55);
  CALL ej2v1(92,23,55);
  CALL ej2v1(-12,-23,-55);

-- ej2v2. Realizar un procedimiento almacenado que reciba tres números 
-- y te indique el mayor de ellos, realizarle con una tabla temporal 
-- y una consulta de totales.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v2(numero1 int, numero2 int, numero3 int)
    BEGIN
      -- crear tabla temporal
      CREATE OR REPLACE TEMPORARY TABLE ej2(
        id int AUTO_INCREMENT,
        num int,
        PRIMARY KEY (id)
      );

      -- insertar los datos
      INSERT INTO ej2 (num)
        VALUES (numero1),(numero2),(numero3);

      -- calcular y mostrar el mayor
      SELECT MAX(e.num) FROM ej2 e;
      
    END //
  DELIMITER ;

  CALL ej2v2(12,23,55);

-- ej2v3. Realizar un procedimiento almacenado que reciba tres números 
-- y te indique el mayor de ellos, realizarle con la función de
-- mysql GREATEST.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2v3(numero1 int, numero2 int, numero3 int)
    BEGIN
       -- declarar variable
      DECLARE vmayor int DEFAULT 0;

      -- realizar calculos
      SET vmayor = GREATEST(numero1, numero2, numero3);

      -- mostrar resultado
      SELECT vmayor;
    END //
  DELIMITER ;

  CALL ej2v3(12,23,55);

-- ej3. Realizar un procedimiento almacenado que reciba tres números y
-- dos argumentos de salida donde devuelva el número más grande y el número 
-- más pequeño de los tres números pasados.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej3(numero1 int, numero2 int, numero3 int, OUT vmayor int, OUT vmenor int)
    BEGIN
      -- realizar calculos y guardarlos en las variables de salida
      SET vmayor = GREATEST(numero1, numero2, numero3);
      SET vmenor = LEAST(numero1, numero2, numero3);

    END //
  DELIMITER ;

  CALL ej3(12,23,55,@mayor,@menor);
  SELECT @mayor,@menor;

-- ej4. Realizar un procedimiento almacenado que reciba dos fechas y
-- te muestre el número de días de diferencia entre las dos fechas.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4(fecha1 date, fecha2 date)
    BEGIN
      -- declarar variables 
      DECLARE vdias int DEFAULT 0;

      -- realizar calculos
      SET vdias = DATEDIFF(fecha2,fecha1);

      -- mostrar resultados
      SELECT vdias;
    END //
  DELIMITER ;

  CALL ej4('2010/1/1','2010/1/10');

-- ej5. Realizar un procedimiento almacenado que reciba dos fechas y
-- te muestre el número de meses de diferencia entre las dos fechas.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej5(fecha1 date, fecha2 date)
    BEGIN
      -- declarar variables 
      DECLARE vmeses int DEFAULT 0;

      -- realizar calculos
      SET vmeses = TIMESTAMPDIFF(MONTH,fecha1,fecha2);

      -- mostrar resultados
      SELECT vmeses;

    END //
  DELIMITER ;

  CALL ej5('2010/1/1','2010/10/1');

-- ej6. Realizar un procedimiento almacenado que reciba dos fechas y
-- te devuelva en 3 argumentos de salida los dias, meses y años entre 
-- las dos fechas.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT anios int)
    BEGIN
      
      -- realizar calculos y asignar a argumentos de salida
      SET dias = DATEDIFF(fecha2,fecha1);
      SET meses = TIMESTAMPDIFF(MONTH,fecha1,fecha2);
      SET anios = TIMESTAMPDIFF(YEAR,fecha1,fecha2);

    END //
  DELIMITER ;

  CALL ej6('2010/1/1','2020/10/25',@dias,@meses,@anios);
  SELECT @dias,@meses,@anios;
  

-- ej7. Realizar un procedimiento almacenado que reciba una frase y te 
-- muestre el numero de caracteres.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7(frase varchar(150))
    BEGIN
      -- declarar variables
      DECLARE vcaracteres int DEFAULT 0;

      -- realizar calculos
      SET vcaracteres = CHAR_LENGTH(frase);

      -- mostrar resultado
      SELECT vcaracteres;

    END //
  DELIMITER ;

  CALL ej7('ejemplo');